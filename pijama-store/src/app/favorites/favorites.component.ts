import { Component, OnInit, Output, Input, HostBinding,EventEmitter } from '@angular/core';
import { Pijamas } from '../models/pijama.model';
import { faCartPlus, faShoppingCart } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {
  /** font icons */
  faCartPlus= faCartPlus;
  faShoppingCart = faShoppingCart;

  @Input() pijama: Pijamas;
  @Output() clicked: EventEmitter<Pijamas>;

  /** Directiva vinculacion directa de un atributo y una propiedad string */
  @HostBinding('attr.class')cssClass="col-md-6";

  constructor() {
    /* Inicializamos la variable*/
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {}

  
  addCart(){
    this.clicked.emit(this.pijama);
    console.log("add to cart!");
    return false;
  }

  deleteCart(){
    this.pijama.setSelected(false);
  }
  
}
