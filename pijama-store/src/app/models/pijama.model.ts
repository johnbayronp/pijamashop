export class Pijamas {
    /* Forma de inicializar obj
    title: string;
    url: string;
    descripcion: string;
    
    constructor(nombre: string,url: string,descripcion: string) {
        this.title = nombre;
        this.url = url;
        this.descripcion = descripcion;
    } */
    private selected:boolean;
    constructor(public title:string,public url:string,public descripcion:string) {}
    /** getter */
    isSelected():boolean { 
        return this.selected; 
    }
    
    /** setter  */
    setSelected(a:boolean) { 
        this.selected = a
    }
}
