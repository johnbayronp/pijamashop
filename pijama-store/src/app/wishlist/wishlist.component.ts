import { Component, OnInit, Output } from '@angular/core';
import { Pijamas } from '../models/pijama.model';
@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {

  @Output() items: Pijamas[];

  void: boolean;

  constructor() {
    this.items = [];
    this.void = true;
  }

  ngOnInit(): void {
    this.items.forEach(pijama => pijama.setSelected(false));
  }

  save(nombre,url,desc){
    this.items.push(new Pijamas(nombre.value,url.value,desc.value));
    this.void = false;
  }

  addCart(item: Pijamas){
    item.setSelected(true);
  }

  deleteAll(){
    this.items=[];
    this.void = true;
  }

}
